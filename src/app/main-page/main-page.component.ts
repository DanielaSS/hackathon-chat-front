
import { Component, ChangeDetectorRef, OnInit, ViewChild, ViewChildren, ElementRef, QueryList, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { RestServiceService } from "../rest-service.service";
import { MatSnackBar,MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})

export class MainPageComponent implements OnInit, AfterViewInit {
  messagesNotUnderstanting = ["Podrías repetirme?", "Disculpa, ¿podrías repetir?", 
  "Lo siento, me temo que no te entiendo.", 
  "Seguro mi amiga NATALIA PARRA podrá ayudarte 3029297899.", 
  "Intenta comunicándote con BBVA Call Center 4010000"]

  @ViewChild("scrollBottom", {static: false}) private scrollBottom: ElementRef;
  @ViewChildren("messageContainer") messageContainers: QueryList<ElementRef>;

  showChat = false;
  showRegister = true;
  cellphone = "";
  messagesError = [];
  showDivError = false;
  messages=[];
  messageContent="";
  customerName = "";
  isLogin = false; 
  isToken = false;
  state = 1;

  //1: campañas, 2: empleados, 3: oferta.
  categoria=0;

  constructor(public rest: RestServiceService, 
    private route: ActivatedRoute, 
    private router: Router, 
    private changeDetectorRef: ChangeDetectorRef,
    public snackBar: MatSnackBar) {

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.scrollToBottom(); // For messsages already present
    this.messageContainers.changes.subscribe((list: QueryList<ElementRef>) => {
      this.scrollToBottom(); // For messages added later
    });
  }

  scrollToBottom(): void {
    try {
      this.scrollBottom.nativeElement.scrollTop = this.scrollBottom.nativeElement.scrollHeight;
    } catch(err) { 
      //console.log(err);
    }
  }

  loginChat(){
    this.messagesError=[];
    if(this.cellphone.length==10){
      this.showDivError =false;
      this.showChat = true;
      this.showRegister = false;
      this.rest.getRequest("customer/"+this.cellphone+"/").subscribe(
        (data:any) => {
          this.isLogin=true;
          this.customerName=data.Nombres;
          this.messages.push({
            action:"JOINED",
            content:"",
            from:{
              id: 0,
              name: data.Nombres,
              avatar: "assets/images/client.png"
              }
            })
            for (let i = 0; i <data.Mensaje.length; i++) {
              this.messages.push({
                action:"MESSAGE",
                content:data.Mensaje[i],
                from:{
                  id: 1212,
                  name: "Makoto",
                  avatar: "assets/images/chatbot.png"
                  }
              });
            }
          this.state=1;
          console.log("DATA..", data);
        },
        (error) => {
          this.customerName = "Anónimo "+Math.floor(Math.random()); 
          this.isLogin=false;
          console.error("ERROR..", error);
          this.messages.push({
            action:"JOINED",
            content:"",
            from:{
              id: 0,
              name: this.customerName,
              avatar: "assets/images/client.png"
              }
            })
          this.messages.push({
            action:"MESSAGE",
            content: "Hola, Soy Makoto. El asistente BBVA.¿Cómo puedo ayudarte?",
            from:{
              id: 1212,
              name: "Makoto",
              avatar: "assets/images/chatbot.png"
              }
          });
          this.messages.push({
            action:"MESSAGE",
            content: "Con información sobre campañas, empleados o oferta de productos.",
            from:{
              id: 1212,
              name: "Makoto",
              avatar: "assets/images/chatbot.png"
              }
          });
          this.messages.push({
            action:"MESSAGE",
            content: "Incluye las palabras.",
            from:{
              id: 1212,
              name: "Makoto",
              avatar: "assets/images/chatbot.png"
              }
          });
        }
      );
    }else{
        if(this.cellphone.length!=10){
          this.messagesError.push("Por favor, verifica tu número de celular.")
        }
        this.showDivError=true;
    }
  }

  sendMessage(){
    if(this.messageContent.length>0){
      this.messages.push({
        action:"MESSAGE",
        content:this.messageContent,
        from:{
          id: 1212,
          name: this.customerName,
          avatar: "assets/images/client.png"
          }
        })
      if(!this.isLogin){ 
        this.filterMessage();
      }else{
        var body:any={}
        if(this.isToken){
          body = { token: this.messageContent , state: this.state }
        }else{
          body = { response: this.messageContent, state: this.state }
        }
        if(this.state == 3){
          this.state = 4;
        }
        console.log("DATA LOAD....", body)
        this.rest.postRequest("customer/"+this.cellphone+"/answer/",JSON.stringify(body)).subscribe(
          (data)=>{
            console.log(data, "RESPONSE: ")
            if(data.Token.length != 0){
              this.isToken = true;
              this.openSnackBar("El código es "+data.Token, "Ok");
              this.state = 2;
            }else{
              this.isToken = false;
              this.state = 3
            }
            for (let i = 0; i <data.Mensaje.length; i++) {
              this.messages.push({
                action:"MESSAGE",
                content:data.Mensaje[i],
                from:{
                  id: 1212,
                  name: "Makoto",
                  avatar: "assets/images/chatbot.png"
                  }
              });
            }
            //console.log("DATA..",data);
          },
          (error) => {
            var index = Math.floor(Math.random() * (this.messagesNotUnderstanting.length - 0) + 0);
            console.log(index);
            this.messages.push({
              action:"MESSAGE",
              content:this.messagesNotUnderstanting[index],
              from:{
                id: 1212,
                name: "Makoto",
                avatar: "assets/images/chatbot.png"
                }
            });
            console.error("ERROR..", error);
          }
        );
      }
      this.messageContent="";
      this.scrollToBottom();
    }
  }

  filterMessage(){
    var response = this.messageContent.split(" ");
    for (let i = 0; i <response.length; i++) {
      if(response[i].toUpperCase() == "CAMPAÑAS"){
        this.categoria = 1;
        this.messages.push({
          action:"MESSAGE",
          content: "Estás son nuestras campañas vigentes:",
          from:{
            id: 1212,
            name: "Makoto",
            avatar: "assets/images/chatbot.png"
            }
        });
        this.rest.getRequest('campaigns/').subscribe(
          (data)=>{
            for (let i = 0; i <response.length; i++) {
              this.messages.push({
                action:"MESSAGE",
                content: "Nombre: "+data[i].name,
                from:{
                  id: 1212,
                  name: "Makoto",
                  avatar: "assets/images/chatbot.png"
                  }
              });
            }
            
          },(error) => {
            var index = Math.floor(Math.random() * (this.messagesNotUnderstanting.length - 0) + 0);
            console.log(index);
            this.messages.push({
              action:"MESSAGE",
              content:this.messagesNotUnderstanting[index],
              from:{
                id: 1212,
                name: "Makoto",
                avatar: "assets/images/chatbot.png"
                }
            });
            console.error("ERROR..", error);
          });
        
      }else if(response[i].toUpperCase() == "EMPLEADOS"){
        this.categoria = 2;
        this.messages.push({
          action:"MESSAGE",
          content: "Nuestro asesor CAROLINA PEREZ, con el número 310 290 9029",
          from:{
            id: 1212,
            name: "Makoto",
            avatar: "assets/images/chatbot.png"
            }
        });
        this.messages.push({
          action:"MESSAGE",
          content: "Te contactará en breve.",
          from:{
            id: 1212,
            name: "Makoto",
            avatar: "assets/images/chatbot.png"
            }
        });
      }else if(response[i].toUpperCase() == "PRODUCTOS"){
        this.categoria = 3;
          this.messages.push({
            action:"MESSAGE",
            content: "Registramos tu solicitud. Nuestros Asesores te contactarán en breve. Para que seas cliente BBVA.",
            from:{
              id: 1212,
              name: "Makoto",
              avatar: "assets/images/chatbot.png"
              }
          });
      }
    }
    this.messageContent="";
  }

  openSnackBar(message: string, action: string) {
    const config = new MatSnackBarConfig();
    config.duration = 10000;
    if(action=="Ok"){
        config.panelClass = ['sucessMessage'];
    }else{
        config.panelClass = ['errorMessage'];
    }
    this.snackBar.open(message, action, config);
  }


}
