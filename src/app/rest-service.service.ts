import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

const endpoint = 'http://localhost:8000/';

@Injectable({
  providedIn: 'root'
})

export class RestServiceService {

  headers: HttpHeaders;

  constructor(private http: HttpClient, private router: Router) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Access-Control-Allow-Methods', 'GET,POST,PUT');
  }

  getRequest(route: string): Observable<any> {
      return this.http.get(endpoint + route, { headers: this.headers });
  }

  postRequest(route: string, body: any): Observable<any> {
      return this.http.post(endpoint + route, body, { headers: this.headers });
  }

  putRequest(route:string, body:any): Observable<any> {
      return this.http.put(endpoint + route, body, { headers: this.headers });
  }

}
